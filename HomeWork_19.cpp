#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animal voice! \n";
    }
};

class Dog:public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }

};

class Cat :public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }

};

class Cow :public Animal
{
public:
    void Voice() override
    {
        std::cout << "Muuuu!\n";
    }

};

class Goat :public Animal
{
public:
    void Voice() override
    {
        std::cout << "Beee!\n";
    }

};


int main()
{
    Animal* p[4];
    p[0] = new Dog;
    p[1] = new Cat;
    p[2] = new Cow;
    p[3] = new Goat;
    for (int i = 0; i < 4; i++)
    {
        p[i]->Voice();
    }
}

